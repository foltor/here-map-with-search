<?php

if(isset($_POST['location'])){
  $location = $_POST['location'];
}else{
  $location = "Malaysia";
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="initial-scale=1.0, width=device-width" />
  <script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js" type="text/javascript" ></script>
  <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
</head>
<body>
  <form name="search" id="search" method="POST">
    <input type="text" name="location" id="location" size="90" value="<?php echo $location; ?>" />
    <input type="submit">
  </form>
  <br/><br/>
  <div style="width: 1000px; height: 800px" id="map"></div>
  <script>
  /**
   * Calculates and displays the address details of 200 S Mathilda Ave, Sunnyvale, CA
   * based on a free-form text
   *
   *
   * A full list of available request parameters can be found in the Geocoder API documentation.
   * see: http://developer.here.com/rest-apis/documentation/geocoder/topics/resource-geocode.html
   *
   * @param   {H.service.Platform} platform    A stub class to access HERE services
   */
  function geocode(platform) {
    var geocoder = platform.getGeocodingService(),
      geocodingParameters = {
        searchText: '<?php echo $location;?>',
        jsonattributes : 1
      };

    geocoder.geocode(
      geocodingParameters,
      onSuccess,
      onError
    );
  }
  /**
   * This function will be called once the Geocoder REST API provides a response
   * @param  {Object} result          A JSONP object representing the  location(s) found.
   *
   * see: http://developer.here.com/rest-apis/documentation/geocoder/topics/resource-type-response-geocode.html
   */
  function onSuccess(result) {
    var locations = result.response.view[0].result;
   /*
    * The styling of the geocoding response on the map is entirely under the developer's control.
    * A representitive styling can be found the full JS + HTML code of this example
    * in the functions below:
    */
    addLocationsToMap(locations);
    // addLocationsToPanel(locations);
    // ... etc.
  }

  /**
   * This function will be called if a communication error occurs during the JSON-P request
   * @param  {Object} error  The error message received.
   */
  function onError(error) {
    alert('Can\'t reach the remote server');
  }



  /**
   * Boilerplate map initialization code starts below:
   */

  //Step 1: initialize communication with the platform
  // In your own code, replace variable window.apikey with your own apikey
  var platform = new H.service.Platform({
    'apikey': 'yourKey'
  });

  function addMarkerToGroup(group, coordinate, html) {
    var outerElement = document.createElement('div'),
        innerElement = document.createElement('div');

    outerElement.style.userSelect = 'none';
    outerElement.style.webkitUserSelect = 'none';
    outerElement.style.msUserSelect = 'none';
    outerElement.style.mozUserSelect = 'none';
    outerElement.style.cursor = 'default';

    innerElement.style.color = 'red';
    innerElement.style.backgroundColor = 'red';
    innerElement.style.border = '2px solid black';
    innerElement.style.font = 'normal 12px arial';
    innerElement.style.lineHeight = '12px'

    innerElement.style.paddingTop = '2px';
    innerElement.style.paddingLeft = '4px';
    innerElement.style.width = '1em';
    innerElement.style.height = '1em';

    // add negative margin to inner element
    // to move the anchor to center of the div
    innerElement.style.marginTop = '-10px';
    innerElement.style.marginLeft = '-10px';

    outerElement.appendChild(innerElement);


    function changeOpacity(evt) {
      evt.target.style.opacity = 0.6;
    };

    function changeOpacityToOne(evt) {
      evt.target.style.opacity = 1;
    };

    //create dom icon and add/remove opacity listeners
    var domIcon = new H.map.DomIcon(outerElement, {
      // the function is called every time marker enters the viewport
      onAttach: function(clonedElement, domIcon, domMarker) {
        clonedElement.addEventListener('mouseover', changeOpacity);
        clonedElement.addEventListener('mouseout', changeOpacityToOne);
      },
      // the function is called every time marker leaves the viewport
      onDetach: function(clonedElement, domIcon, domMarker) {
        clonedElement.removeEventListener('mouseover', changeOpacity);
        clonedElement.removeEventListener('mouseout', changeOpacityToOne);
      }
    });

    var marker = new H.map.DomMarker(coordinate, {icon: domIcon});
    // add custom data to the marker
    marker.setData(html);
    group.addObject(marker);
  }

  function addInfoBubble(map) {
    var group = new H.map.Group();

    map.addObject(group);

    // add 'tap' event listener, that opens info bubble, to the group
    group.addEventListener('tap', function (evt) {
      // event target is the marker itself, group is a parent event target
      // for all objects that it contains
      var bubble =  new H.ui.InfoBubble(evt.target.getGeometry(), {
        // read custom data
        content: evt.target.getData()
      });
      // show info bubble
      ui.addBubble(bubble);
    }, false);


    addMarkerToGroup(group, {lat:53.439, lng:-2.221},
      '<div><a href="http://www.mcfc.co.uk" target="_blank">Manchester City</a>' +
      '</div><div >City of Manchester Stadium<br>Capacity: 48,000</div>');

      addMarkerToGroup(group, {lat:53.430, lng:-2.961},
        '<div><a href="http://www.liverpoolfc.tv" target="_blank">Liverpool</a>' +
        '</div><div >Anfield<br>Capacity: 45,362</div>');

      }
  var defaultLayers = platform.createDefaultLayers();

  //Step 2: initialize a map - this map is centered over California
  var map = new H.Map(document.getElementById('map'),
    defaultLayers.vector.normal.map,{
    center: {lat:53.439, lng:-2.221},
    zoom: 10,
    pixelRatio: window.devicePixelRatio || 1
  });
  // add a resize listener to make sure that the map occupies the whole container
  window.addEventListener('resize', () => map.getViewPort().resize());


  //Step 3: make the map interactive
  // MapEvents enables the event system
  // Behavior implements default interactions for pan/zoom (also on mobile touch environments)
  var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));

  // Create the default UI components
  var ui = H.ui.UI.createDefault(map, defaultLayers);

  // Hold a reference to any infobubble opened
  var bubble;

  /**
   * Opens/Closes a infobubble
   * @param  {H.geo.Point} position     The location on the map.
   * @param  {String} text              The contents of the infobubble.
   */
  // function openBubble(position, text){
  //  if(!bubble){
  //     bubble =  new H.ui.InfoBubble(
  //       position,
  //       {content: text});
  //     ui.addBubble(bubble);
  //   } else {
  //     bubble.setPosition(position);
  //     bubble.setContent(text);
  //     bubble.open();
  //   }
  // }



  /**
   * Creates a series of H.map.Markers for each location found, and adds it to the map.
   * @param {Object[]} locations An array of locations as received from the
   *                             H.service.GeocodingService
   */
  function addLocationsToMap(locations){
    var group = new  H.map.Group(),
      position,
      i;

    // Add a marker for each location found
    for (i = 0;  i < locations.length; i += 1) {
      position = {
        lat: locations[i].location.displayPosition.latitude,
        lng: locations[i].location.displayPosition.longitude
      };
      marker = new H.map.Marker(position);
      marker.label = locations[i].location.address.label;
      group.addObject(marker);
    }


    // Add the locations group to the map
    // map.addObject(group);
    map.setCenter(group.getBoundingBox().getCenter());

  }







  // Now use the map as required...
   addInfoBubble(map);
    geocode(platform);
      </script>
    </body>
    </html>
